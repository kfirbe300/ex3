// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase:{
    apiKey: "AIzaSyBEFiTO7-RiK9CWNSqSIc-AHvdGpIQB8is",
    authDomain: "ex04-44123.firebaseapp.com",
    databaseURL: "https://ex04-44123.firebaseio.com",
    projectId: "ex04-44123",
    storageBucket: "ex04-44123.appspot.com",
    messagingSenderId: "750628170967"
    }
  };


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
